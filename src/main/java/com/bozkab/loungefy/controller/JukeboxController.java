package com.bozkab.loungefy.controller;

import com.bozkab.loungefy.model.PlayingTrack;
import com.bozkab.loungefy.spotify.Playlist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jukebox")
public class JukeboxController {

    @Autowired
    private Playlist playlist;

    @PostMapping("/add")
    public boolean addTrack(@RequestParam(name = "spotifyURI") String spotifyURI){
        playlist.addTrack(new PlayingTrack());
        return true;
    }
}
