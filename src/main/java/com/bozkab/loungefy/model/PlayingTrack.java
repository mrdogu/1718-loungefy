package com.bozkab.loungefy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PlayingTrack {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private float id;
    private LoungeUser addedBy;
    private String name;
    private String artist;
    private String spotifyURI;

    public PlayingTrack() {
    }

    @Override
    public boolean equals(Object compared){
        return ((PlayingTrack)compared).getSpotifyURI().equals(getSpotifyURI());
    }

    public String toString() {
        return "PlayingTrack(id=" + this.getId() + ", addedBy=" + this.getAddedBy() + ", name=" + this.getName() + ", artist=" + this.getArtist() + ", spotifyURI=" + this.getSpotifyURI() + ")";
    }

    public float getId() {
        return this.id;
    }

    public LoungeUser getAddedBy() {
        return this.addedBy;
    }

    public String getName() {
        return this.name;
    }

    public String getArtist() {
        return this.artist;
    }

    public String getSpotifyURI() {
        return this.spotifyURI;
    }

    public void setId(float id) {
        this.id = id;
    }

    public void setAddedBy(LoungeUser addedBy) {
        this.addedBy = addedBy;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setSpotifyURI(String spotifyURI) {
        this.spotifyURI = spotifyURI;
    }
}
