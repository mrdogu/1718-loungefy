package com.bozkab.loungefy.repository;

import com.bozkab.loungefy.model.PlayingTrack;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayingTrackRepository extends JpaRepository<PlayingTrack,Float> {

}
