package com.bozkab.loungefy;

import com.bozkab.loungefy.spotify.Playlist;
import com.bozkab.loungefy.spotify.SpotifyConnect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LoungefyApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoungefyApplication.class, args);
    }

    @Bean
    public Playlist playlist(){
        return new Playlist();
    }

    @Bean
    public SpotifyConnect spotifyConnect(){
        return new SpotifyConnect();
    }
}
