package com.bozkab.loungefy.spotify;

import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.miscellaneous.CurrentlyPlayingContext;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.data.player.GetInformationAboutUsersCurrentPlaybackRequest;
import com.wrapper.spotify.requests.data.player.GetUsersCurrentlyPlayingTrackRequest;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;

public class SpotifyConnect {

    private SpotifyApi spotifyApi;

    public void setToken(String token) {
        spotifyApi = new SpotifyApi.Builder()
                .setAccessToken(token)
                .build();
    }

    public Track currentlyPlaying() throws IOException, SpotifyWebApiException {
        GetInformationAboutUsersCurrentPlaybackRequest getInformationAboutUsersCurrentPlaybackRequest =
                spotifyApi.getInformationAboutUsersCurrentPlayback()
                        .market(CountryCode.AT)
                        .build();
            final CurrentlyPlayingContext currentlyPlayingContext = getInformationAboutUsersCurrentPlaybackRequest
                    .execute();
            return currentlyPlayingContext.getItem();
    }

    public int getProgress() throws IOException, SpotifyWebApiException {
        GetUsersCurrentlyPlayingTrackRequest request =
                spotifyApi.getUsersCurrentlyPlayingTrack()
                        .market(CountryCode.AT)
                        .build();
        var currentlyPlayingContext = request
                .execute();
        return currentlyPlayingContext.getProgress_ms();
    }

    public boolean isCurrentlyPlaying() {
        var getInformationAboutUsersCurrentPlaybackRequest =
                spotifyApi.getInformationAboutUsersCurrentPlayback()
                        .market(CountryCode.AT)
                        .build();
        try {
            final CurrentlyPlayingContext currentlyPlayingContext = getInformationAboutUsersCurrentPlaybackRequest
                    .execute();
            return currentlyPlayingContext.getIs_playing();
        } catch (IOException | SpotifyWebApiException e) {

            return false;
        }
    }

    public Track[] getSongResults(String songName) throws SpotifyWebApiException, IOException {
        var request = spotifyApi.searchTracks(songName).build();
        return request.execute().getItems();
    }

}
