package com.bozkab.loungefy.spotify;

import com.bozkab.loungefy.model.PlayingTrack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;

public class Playlist {

    @Autowired
    private SpotifyConnect spotifyConnect;
    private ArrayList<PlayingTrack> playQueue;

    public Playlist(){
        playQueue = new ArrayList<>();
    }

    public void addTrack(PlayingTrack newTrack){
        playQueue.add(newTrack);
    }

    public void removeTrack(PlayingTrack removed){
        playQueue.remove(removed);
    }

    @Scheduled(cron = "*/1 * * * * *")
    public void checkProgress() {

    }

}
