package com.bozkab.loungefy.repository;

import com.bozkab.loungefy.model.LoungeUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoungeUserRepository extends JpaRepository<LoungeUser, Float> {
    LoungeUser findByName(String name);
}
