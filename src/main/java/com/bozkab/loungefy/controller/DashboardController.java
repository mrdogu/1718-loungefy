package com.bozkab.loungefy.controller;

import com.bozkab.loungefy.repository.LoungeUserRepository;
import com.bozkab.loungefy.spotify.SpotifyConnect;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/dashboard")
public class DashboardController {

    private Track currentlyPlaying;
    private int playingProgress;

    @Autowired
    private SpotifyConnect spotifyConnect;

    @Scheduled(" */1 * * * *")

    @GetMapping("/current")
    public Track currentPlayingTrack(){
        try {
            return spotifyConnect.currentlyPlaying();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SpotifyWebApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("/progress")
    public int getProgress() {
        try {
            return spotifyConnect.getProgress();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SpotifyWebApiException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
