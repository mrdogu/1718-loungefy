package com.bozkab.loungefy.controller;


import com.bozkab.loungefy.spotify.SpotifyConnect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@RestController
@RequestMapping("/spotifyAuth")
public class SpotifyAuthSpringController {

    @Autowired
    private SpotifyConnect spotifyConnect;

    @GetMapping("/")
    public void sendToAuth(HttpServletResponse response) throws IOException {
        String[] scopes = new String[]{"user-library-read","user-library-modify","streaming","user-modify-playback-state","user-read-currently-playing","user-read-playback-state"};
        String clientID = "60cd3270b49549f98ace6475c8b332b2";
        String redirectURL = "https://accounts.spotify.com/authorize" +
                "?response_type=token" +
                "&client_id=" + clientID +
                "&redirect_uri=" + URLEncoder.encode("http://localhost:8080/spotifyAuth/token")+
                "&scope="+ URLEncoder.encode(String.join(" ", scopes));

        response.sendRedirect(redirectURL);
    }

    @GetMapping("/token")
    public String fetchAuth(HttpServletResponse response, @RequestParam(name = "access_token") String token) throws IOException {
        spotifyConnect.setToken(token);
        return token;
        //response.sendRedirect("http://localhost:8080/dashboard/current");
    }

}
