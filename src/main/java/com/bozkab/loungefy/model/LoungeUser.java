package com.bozkab.loungefy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LoungeUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public float id;
    public String name;

    public LoungeUser() {
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof LoungeUser)) return false;
        final LoungeUser other = (LoungeUser) o;
        if (!other.canEqual((Object) this)) return false;
        if (Float.compare(this.getId(), other.getId()) != 0) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * PRIME + Float.floatToIntBits(this.getId());
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 43 : $name.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof LoungeUser;
    }

    public String toString() {
        return "LoungeUser(id=" + this.getId() + ", name=" + this.getName() + ")";
    }

    public float getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(float id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
